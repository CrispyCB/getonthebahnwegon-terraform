resource "aws_acm_certificate" "simpledevelopersurvey" {
  domain_name       = "simpledevelopersurvey.getonthebahnwegon.com"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "designsofdieterrams" {
  domain_name       = "designsofdieterrams.getonthebahnwegon.com"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "getonthebahnwegon_website" {
  domain_name       = aws_route53_record.getonthebahnwegon_a_record.name
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "getonthebahnwegon_cert_validation" {
  for_each = {
    for dvo in aws_acm_certificate.getonthebahnwegon_website.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.getonthebahnwegon.zone_id
}

resource "aws_acm_certificate_validation" "getonthebahnwegon_website" {
  certificate_arn         = aws_acm_certificate.getonthebahnwegon_website.arn
  validation_record_fqdns = [for record in aws_route53_record.getonthebahnwegon_cert_validation : record.fqdn]
}