resource "aws_route53_zone" "getonthebahnwegon" {
  name = "getonthebahnwegon.com"
}

resource "aws_route53_record" "getonthebahnwegon_a_record" {
  zone_id = aws_route53_zone.getonthebahnwegon.zone_id
  name    = "getonthebahnwegon.com"
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.getonthebahnwegon_website.domain_name
    zone_id                = aws_cloudfront_distribution.getonthebahnwegon_website.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www_getonthebahnwegon_a_record" {
  zone_id = aws_route53_zone.getonthebahnwegon.zone_id
  name    = "www.getonthebahnwegon.com"
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.getonthebahnwegon_website.domain_name
    zone_id                = aws_cloudfront_distribution.getonthebahnwegon_website.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "testing_getonthebahnwegon_a_record" {
  zone_id = aws_route53_zone.getonthebahnwegon.zone_id
  name    = "testing.getonthebahnwegon.com"
  type    = "A"
  ttl     = 300
  records = ["240.0.0.0"]
}

resource "aws_route53_record" "designsofdieterrams_a_record" {
  zone_id = aws_route53_zone.getonthebahnwegon.zone_id
  name    = "designsofdieterrams.getonthebahnwegon.com"
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.designsofdieterrams.domain_name
    zone_id                = "Z2FDTNDATAQYW2"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "designsofdieterrams_cname_record" {
  zone_id = aws_route53_zone.getonthebahnwegon.zone_id
  name    = "_b3c38f8c66dca0b5cf4abb1c649e1423.designsofdieterrams.getonthebahnwegon.com"
  type    = "CNAME"
  ttl     = 300
  records = ["_5b52680d28ed1852dbe3eba5e4f557bc.tfmgdnztqk.acm-validations.aws."]
}

resource "aws_route53_record" "simpledevelopersurvey_a_record" {
  zone_id = aws_route53_zone.getonthebahnwegon.zone_id
  name    = "simpledevelopersurvey.getonthebahnwegon.com"
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.simpledevelopersurvey.domain_name
    zone_id                = "Z2FDTNDATAQYW2"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "simpledevelopersurvey_cname_record" {
  zone_id = aws_route53_zone.getonthebahnwegon.zone_id
  name    = "_7c0e9f3fc4577053855587d70623e292.simpledevelopersurvey.getonthebahnwegon.com"
  type    = "CNAME"
  ttl     = 300
  records = ["_7103131783df305e56dce0f34202cb2b.zbkrxsrfvj.acm-validations.aws."]
}