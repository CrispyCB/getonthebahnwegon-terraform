resource "aws_cloudfront_distribution" "designsofdieterrams" {
  origin {
    connection_attempts = 3
    connection_timeout  = 10
    domain_name         = "designsofdieterrams.getonthebahnwegon.com.s3.amazonaws.com"
    origin_id           = "S3-designsofdieterrams.getonthebahnwegon.com"

    s3_origin_config {
      origin_access_identity = "origin-access-identity/cloudfront/E3FELRNLLBYHF3"
    }
  }
  aliases = [
    "designsofdieterrams.getonthebahnwegon.com",
  ]
  default_root_object = "index.html"
  enabled             = true
  is_ipv6_enabled     = true
  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "S3-designsofdieterrams.getonthebahnwegon.com"
    viewer_protocol_policy = "redirect-to-https"

    forwarded_values {
      headers                 = []
      query_string            = false
      query_string_cache_keys = []

      cookies {
        forward           = "none"
        whitelisted_names = []
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }

  viewer_certificate {
    acm_certificate_arn      = "arn:aws:acm:us-east-1:083893797876:certificate/5a1a3e50-26dc-4930-9260-02ece76fc378"
    minimum_protocol_version = "TLSv1.2_2018"
    ssl_support_method       = "sni-only"
  }
}

resource "aws_cloudfront_distribution" "simpledevelopersurvey" {
  origin {
    connection_attempts = 3
    connection_timeout  = 10
    domain_name         = "simpledevelopersurvey.getonthebahnwegon.com.s3.amazonaws.com"
    origin_id           = "S3-simpledevelopersurvey.getonthebahnwegon.com"

    s3_origin_config {
      origin_access_identity = "origin-access-identity/cloudfront/E1SER8S9RP0KDG"
    }
  }
  aliases = ["simpledevelopersurvey.getonthebahnwegon.com",
  ]
  default_root_object = "index.html"
  is_ipv6_enabled     = true

  enabled = true
  default_cache_behavior {
    allowed_methods = [
      "GET",
      "HEAD",
    ]
    cached_methods = [
      "GET",
      "HEAD",
    ]
    cache_policy_id        = "658327ea-f89d-4fab-a63d-7e88639e58f6"
    target_origin_id       = "S3-simpledevelopersurvey.getonthebahnwegon.com"
    viewer_protocol_policy = "allow-all"

  }

  ordered_cache_behavior {
    allowed_methods = [
      "GET",
      "HEAD",
    ]
    cached_methods = [
      "GET",
      "HEAD",
    ]
    cache_policy_id        = "658327ea-f89d-4fab-a63d-7e88639e58f6"
    compress               = false
    default_ttl            = 0
    max_ttl                = 0
    min_ttl                = 0
    path_pattern           = "thanks"
    smooth_streaming       = false
    target_origin_id       = "S3-simpledevelopersurvey.getonthebahnwegon.com"
    trusted_key_groups     = []
    trusted_signers        = []
    viewer_protocol_policy = "allow-all"

    lambda_function_association {
      event_type   = "origin-request"
      include_body = false
      lambda_arn   = "arn:aws:lambda:us-east-1:083893797876:function:Redirect:1"
    }
  }


  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }

  viewer_certificate {
    minimum_protocol_version = "TLSv1.2_2019"
    ssl_support_method       = "sni-only"
    acm_certificate_arn      = "arn:aws:acm:us-east-1:083893797876:certificate/8a26373b-04a6-436b-9571-8ce098fb48d5"

  }

}

resource "aws_cloudfront_distribution" "getonthebahnwegon_website" {
  enabled         = true
  is_ipv6_enabled = true
  aliases = [
    "getonthebahnwegon.com"
  ]

  origin {
    domain_name = aws_s3_bucket_website_configuration.getonthebahnwegon_website.website_endpoint
    origin_id   = aws_s3_bucket.getonthebahnwegon_website.bucket_regional_domain_name

    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_keepalive_timeout = 5
      origin_protocol_policy   = "http-only"
      origin_read_timeout      = 30
      origin_ssl_protocols = [
        "TLSv1.2",
      ]
    }
  }

  viewer_certificate {
    acm_certificate_arn      = "arn:aws:acm:us-east-1:083893797876:certificate/2529a343-abc1-4dff-a904-9e498ce7ea9d"
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2018"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }

  default_cache_behavior {
    cache_policy_id        = "658327ea-f89d-4fab-a63d-7e88639e58f6"
    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    allowed_methods        = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = aws_s3_bucket.getonthebahnwegon_website.bucket_regional_domain_name
  }
}