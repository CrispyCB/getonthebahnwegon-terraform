resource "aws_s3_bucket" "designsofdieterrams" {
  bucket = "designsofdieterrams.getonthebahnwegon.com"
}

resource "aws_s3_bucket" "dieter-rams-selected-designs" {
  bucket = "dieter-rams-selected-designs"
}

resource "aws_s3_bucket" "getonthebahnwegon" {
  bucket = "getonthebahnwegon"
}

resource "aws_s3_bucket" "simpledevelopersurvey" {
  bucket = "simpledevelopersurvey.getonthebahnwegon.com"
}

resource "aws_s3_bucket" "getonthebahnwegon_website" {
  bucket = "getonthebahnwegon.com"
}

resource "aws_s3_bucket_website_configuration" "getonthebahnwegon_website" {
  bucket = aws_s3_bucket.getonthebahnwegon_website.id

  index_document {
    suffix = "index.html"
  }
}

resource "aws_s3_bucket_public_access_block" "getonthebahnwegon_website_public_access_block" {
  bucket = aws_s3_bucket.getonthebahnwegon_website.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_policy" "getonthebahnwegon_website_access_policy" {
  bucket = aws_s3_bucket.getonthebahnwegon_website.id
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "PublicReadGetObject",
        "Effect" : "Allow",
        "Principal" : "*",
        "Action" : "s3:GetObject",
        "Resource" : "arn:aws:s3:::getonthebahnwegon.com/*"
      },
      {
        "Sid" : "AllowCloudFrontServicePrincipal",
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "cloudfront.amazonaws.com"
        },
        "Action" : "s3:GetObject",
        "Resource" : "arn:aws:s3:::getonthebahnwegon.com/*",
        "Condition" : {
          "StringEquals" : {
            "AWS:SourceArn" : "arn:aws:cloudfront::083893797876:distribution/E3G1ZST6DV9PFX"
          }
        }
      }
    ]
  })
}